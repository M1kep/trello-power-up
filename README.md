# GitLab Trello Power-Up

This repository contains the code used for GitLab's [Trello Power-Up](https://developers.trello.com/power-ups/intro) which was released in the 9.4 milestone.

## Getting Started (Development)

1. `yarn install`
1. `yarn serve` and take note of the https ngrok url
1. Go to [Trello's Power-Ups Admin Console](https://trello.com/power-ups/admin)
1. Select a team to test
1. Fill out `Trello Joint Development Agreement`
1. Select `Create New Power-Up`
1. Fill in the `Power-Up Name` input with an identifiable name (Eg. `GitLab Test`)
1. Enable the following `Power Up Capabilities`
  1. `authorization-status`
  1. `attachment-sections`
  1. `attachment-thumbnail`
  1. `callback`
  1. `card-badges`
  1. `card-buttons`
  1. `show-authorization`
1. Fill in the `Power-Up icon URL` input with the https ngrok url from step 2 with the suffix `images/logo.png` (Eg. ` https://fff56381.ngrok.io/images/logo.png`)
1. Fill in the `Iframe connector URL` input with the https ngrok url from step 2 (Eg. ` https://fff56381.ngrok.io/`)
1. Click `Save`
1. Click `Go to Your Boards` on the top right corner
1. Select a board under the team you designated in step 4
1. Click `Show Menu` on the top right (if the menu is not already opened)
1. Click on `Power-Ups`
1. Scroll down the list and click `Enable` next to the Power-Up named `GitLab Test` (or whatever you named your Power Up)

## Releasing a new version

1. Merge `master` into `production` using a MR (Do not squash commits)
2. After merging, create a new tag with the correct version number

GitLab pages will auto-deploy the changes and it will auto-reflect on Trello's side.
If the new version updates the UI, please reach out to Trello team and have them update their screenshots.

## Features
- Attach merge requests to Trello cards

## Helpful Links

[Trello Power-Up Documentation](https://developers.trello.com/power-ups)

## Contributing
Please refer to [CONTRIBUTING.md](/CONTRIBUTING.md) for details.
